﻿var json_answer;

function openTab(evt, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}

function addFilm()
{
    var xhttp = new XMLHttpRequest();
    var elements = document.getElementsByClassName("addFilm");
    var formData = new FormData(); 
    for(var i=0; i<elements.length; i++)
    {
        formData.append(elements[i].name, elements[i].value);
    }


  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      alert(this.responseText);
      if (this.responseText == "film added") {
        document.getElementById("name").value = '';
        document.getElementById("year").value = '';
        document.getElementById("actors").value = '';
        }
    }
  };
  xhttp.open("POST", "addfilm.php", true);
  xhttp.send(formData);

}

function for_get_json() {
          if (this.readyState == 4 && this.status == 200) {

            var txt = "<tr class=\"header\"><th style=\"width:20%;\">Name</th><th \
            style=\"width:15%;\">Year</th><th style=\"width:15%;\">Format</th><th\
            style=\"width:40%;\">Actors</th><th style=\"width:10%;\">Delete</th></tr>";

            json_answer = JSON.parse(this.responseText);
            for (var x = 0; x < json_answer.length; x++){

                txt += "<tr><td>" + json_answer[x].name +"</td><td>"+ json_answer[x].year + "</td><td>" 
                + json_answer[x].format + "</td><td>" + json_answer[x].actors +
                 "</td><td><img class=\"del-img\" id=\""+json_answer[x].id+
                 "\"src=\"css/del.png\" onclick=\"delet(this.id)\" width=\"80%\" height=\"auto\" ></td></tr>";

                console.log(json_answer[x]);
            }
            document.getElementById("myTable2").innerHTML = txt;
        }
}

window.onload = function() {document.getElementById("2").addEventListener("click",function () {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = for_get_json;
    xhttp.open("GET", "list_of_film.php", true);
    xhttp.send();
});}

function delet(idd){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = for_get_json;
    console.log(idd);
    xhttp.open("GET", "delete.php?id="+idd, true);
    xhttp.send();
}

function f () {
  if (this.readyState == 4 && this.status == 200) {
    alert(this.responseText);
    console.log(this.responseText);
  }
}
function uploadFile() {
  var file=document.getElementById("uploadFile");
  var xhttp = new XMLHttpRequest();
  var form = new FormData();
  var upload = file.files[0];
  xhttp.onreadystatechange = f;
  form.append("file", upload);
  xhttp.open("post", "upload.php", true);
  xhttp.send(form);
}

function find() {
  var input, filter, table, tr, td, i, colm;
  colm = document.querySelector('input[name="status"]:checked').value;
  console.log(colm);
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable2");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[colm];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}