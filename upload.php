<?php
	include_once "DB.class.php";
	if(!isset($_FILES['file']) || !preg_match("/(.*?)\.(txt)$/", $_FILES['file']['name'])
		|| $_FILES['file']['size'] > 10000 || $_FILES['file']['error'] > 0){
		die("bad file");
	}

	$txt = trim(file_get_contents($_FILES["file"]["tmp_name"]));
	$arr = explode("\n", $txt);
	$to_db = array();
	$elem = array();
	if (count($arr) < 4)
		die("wrong text format");
	$flag = array();
	$flag = array_pad($flag, 5,0);
	for($i = 0; $i < count($arr);$i++) {
		if ($i % 5 == 0){
			if(preg_match("/^Title:.{1,240}$/u", $arr[$i]))
				$elem['name'] = trim(substr($arr[$i], 7));
			else{
				$flag[0] = 1;
			}
		}
		else if ($i % 5 == 1 ){
			if (preg_match("/^Release Year:\ [1|2][9|0][0-9]{2}($|\ +$)/", $arr[$i]))
				$elem['year'] = trim(substr($arr[$i], 13));
			else{
				$flag[1] = 1;
			}
		}
		else if ($i %5 == 2){
			if (preg_match("/^Format:\s{1,5}((DVD)|(VHS)|(Blu\-Ray))$/", $arr[$i]))
				$elem['format'] = trim(substr($arr[$i], 8));
			else{
				$flag[2] = 1;
			}
		}
		else if ($i % 5 == 3){
			if (preg_match("/^Stars:.{1,240}$/", $arr[$i]))
				$elem['actors'] = trim(substr($arr[$i], 7));
			else{
				$flag[3] = 1;
			}
		}
		else if ($i % 5 == 4){
			if((strlen(trim($arr[$i])) != 0))
				$flag[4] = 1;
			$to_db[] = $elem;
		}
	}
	$to_db[] = $elem;
	if (in_array(1, $flag)){
		die ("error in file format");
		}
	$str = "";
	$j = 0;
	try {
		$db = new Db($host, $user, $password, $db_name);
		while ($j < count($to_db)) {
			$tmp = "('".$db->escape(trim($to_db[$j]['name']))."', ".$db->escape(trim($to_db[$j]['year'])).
			", '".$db->escape(trim($to_db[$j]['format']))."', '".$db->escape(trim($to_db[$j]['actors']))."'), ";
			$str = $str . $tmp;
			$j++;
		}
		$str = substr($str,0, -2);
		$sql = "INSERT INTO film (name, year, format, actors) 
				VALUES $str";
		$db->query($sql);
		echo "File add to db";
	}
	catch (Exception $e) {
		die("Connection error");
	}
?>

