<?php
	include_once('DB.class.php');
	$to_json  = array();
	try {
		$db = new Db($host, $user, $password, $db_name);
		$sql = "SELECT id, name, year, format, actors FROM film ORDER BY name";
		$answ = $db->query($sql);
		foreach ($answ as $row) {
			array_push($to_json, $row);
		}
		echo json_encode($to_json);
	}
	catch (Exception $e) {
		$to_json['error'] = 1;
		echo json_encode($to_json);
	}
?>