<?php
    $host = "localhost"; 
    $user = "root"; 
    $password = "123123";
    $db_name = "testDB";

    $conn = new mysqli($host, $user, $password);
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 
	$sql = "CREATE DATABASE $db_name";
	if ($conn->query($sql) === TRUE) {
	    echo "Database created successfully";
	} else {
	    echo "Error creating database: " . $conn->error;
	}
	$conn->close();

	$conn = new mysqli($host, $user, $password, $db_name);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 
	
	$sql = "CREATE TABLE film (
	id INT(11) AUTO_INCREMENT PRIMARY KEY, 
	name VARCHAR(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
	year INT(11) ,
	actors VARCHAR(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
	format ENUM('VHS', 'DVD', 'Blu-Ray')
	)";
	
	if ($conn->query($sql) === TRUE) {
	    echo "Table MyGuests created successfully";
	} else {
	    echo "Error creating table: " . $conn->error;
	}

	$conn->close();
?>